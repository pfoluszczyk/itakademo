//
//  IDStatusTextField.swift
//  ItakaDemo
//
//  Created by Przemek on 04.07.2016.
//  Copyright © 2016 Przemek. All rights reserved.
//

import UIKit

class IDStatusTextField: UITextField {
    
    let statusMessageLabel = UILabel()
    
    var statusMessageColor = UIColor.blackColor() {
        didSet {
            statusMessageLabel.textColor = statusMessageColor
        }
    }
    var messageOffset: CGPoint = CGPointMake(0, 0) {
        didSet {
            statusMessageLabel.frame = self.frame
            statusMessageLabel.frame.origin.y += statusMessageLabel.frame.size.height + messageOffset.y
            statusMessageLabel.frame.origin.x += messageOffset.x
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        messageOffset = CGPointMake(0, 5)
        statusMessageColor = .blackColor()
        
        statusMessageLabel.font = self.font
        
        self.superview?.addSubview(statusMessageLabel)
    }
}
