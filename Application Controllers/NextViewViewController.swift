//
//  NextViewViewController.swift
//  ItakaDemo
//
//  Created by Przemek on 04.07.2016.
//  Copyright © 2016 Przemek. All rights reserved.
//

import UIKit

class NextViewViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = NSLocalizedString("Drugi Widok", comment: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
