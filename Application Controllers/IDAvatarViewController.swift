//
//  IDAvatarViewController.swift
//  ItakaDemo
//
//  Created by Przemek on 03.07.2016.
//  Copyright © 2016 Przemek. All rights reserved.
//

import UIKit

class IDAvatarViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private let maxNicknameLength = 15

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var nicknameTextField: IDStatusTextField!
    @IBOutlet weak var bottomButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tnTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(IDAvatarViewController.thumbnailImageViewTapped))
        
        self.title = NSLocalizedString("Główny Widok", comment: "")
        
        thumbnailImageView.layer.borderColor = UIColor.idBorderColor.CGColor
        thumbnailImageView.layer.borderWidth = 2.0
        
        thumbnailImageView.addGestureRecognizer(tnTapGestureRecognizer)
        
        readData()
        
        nicknameTextField.statusMessageLabel.text = "\(nicknameTextField.text?.characters.count ?? 0)/\(maxNicknameLength)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK: - IBActions
    
    @IBAction func nicknameTextFieldDidChanged(sender: AnyObject) {
        guard let textField = sender as? IDStatusTextField else {
            debugPrint("\(self): Unknown textfield provided.")
            
            return
        }
        guard let charactersCount = textField.text?.characters.count else {
            debugPrint("\(self): Can't get characters count.")
            
            return
        }
        
        textField.statusMessageLabel.text = "\(textField.text?.characters.count ?? 0)/\(maxNicknameLength)"
        
        if (charactersCount >= maxNicknameLength) {
            textField.statusMessageColor = .idErrorColor
            textField.layer.borderColor = UIColor.idErrorColor.CGColor
            textField.layer.borderWidth = 1
        } else {
            textField.statusMessageColor = .idTextColor
            textField.layer.borderWidth = 0
        }
        
        saveData()
    }
    
    @IBAction func bottomButtonTapped(sender: AnyObject) {
        let nextViewController = NextViewViewController(nibName: "NextViewViewController", bundle: nil)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Powrót", comment: ""), style: .Plain, target: nil, action: nil)
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
// MARK: - UITextFieldDelegate methods
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case 0:
            guard let charactersCount = textField.text?.characters.count else {
                debugPrint("\(self): Can't get characters count.")
                
                return false
            }
            
            return charactersCount < maxNicknameLength || range.length != 0
            
        default:
            return false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
// MARK: - UIImagePickerControllerDelegate methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        thumbnailImageView.image = image
        
        saveData()
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
// MARK: - Gesture recognizers methods
    
    func thumbnailImageViewTapped() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Anuluj", comment: ""), style: .Cancel) { (uiAlertAction) in
        
        }
        let photoAction = UIAlertAction(title: NSLocalizedString("Zrób zdjęcie", comment: ""), style: .Default) { (uiAlertAction) in
            let imagePicker = UIImagePickerController()
            
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .Camera
            imagePicker.delegate = self
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        let libraryAction = UIAlertAction(title: NSLocalizedString("Biblioteka zdjęć", comment: ""), style: .Default) { (uiAlertAction) in
            let imagePicker = UIImagePickerController()
            
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .PhotoLibrary
            imagePicker.delegate = self
            
            self.presentViewController(imagePicker, animated: true, completion: nil)        }
        
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(photoAction)
        actionSheet.addAction(libraryAction)
        
        self.presentViewController(actionSheet, animated: true) { 
            
        }
    }
    
// MARK: - Data storage methods
    
    func saveData() {
        NSUserDefaults.standardUserDefaults().setObject(nicknameTextField.text, forKey: "idNickname")
        
        if let imagePath = NSUserDefaults.standardUserDefaults().objectForKey("idThumbnailPath") as? String {
            do {
                try NSFileManager.defaultManager().removeItemAtPath(imagePath)
            } catch let error {
                debugPrint("\(self): Cannot delete file at path: \(imagePath), with error: \(error)")
            }
        }
        
        if let image = thumbnailImageView.image {
            let imageData = UIImageJPEGRepresentation(image, 1)
            let relativePath = "idthumbnail_\(NSDate.timeIntervalSinceReferenceDate()).jpg"
            let path = self.documentsPathForFilename(relativePath)
            
            imageData?.writeToFile(path, atomically: true)
            
            NSUserDefaults.standardUserDefaults().setObject(path, forKey: "idThumbnailPath")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func readData() {
        if let imagePath = NSUserDefaults.standardUserDefaults().objectForKey("idThumbnailPath") as? String {
            if let imageData = NSData(contentsOfFile: imagePath) {
                thumbnailImageView.image = UIImage(data: imageData)
            }
        }
        if let nickname = NSUserDefaults.standardUserDefaults().objectForKey("idNickname") as? String {
            nicknameTextField.text = nickname
        }
    }
    
// MARK: - Helper methods
    
    func documentsPathForFilename(filename: String) -> String {
        let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let fullPath = path.stringByAppendingString("/\(filename)")
        
        return fullPath
    }
}