//
//  UIColor.swift
//  ItakaDemo
//
//  Created by Przemek on 04.07.2016.
//  Copyright © 2016 Przemek. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static var idBorderColor: UIColor { return .blackColor() }
    static var idTextColor: UIColor { return .blackColor() }
    static var idErrorColor: UIColor { return .redColor() }
}